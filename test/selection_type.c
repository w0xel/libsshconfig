#include "sshconfig.h"
#include <assert.h>
#include <string.h>

int main() {
	SshSelectionType ty = sshconfig_key_get_selection_type(SSH_KEY_CIPHER);
	assert(ty == SSH_SELECTION_TYPE_CIPHER && "Wrong selection type");

	ty = sshconfig_key_get_selection_type(SSH_KEY_CIPHERS);
	assert(ty == SSH_SELECTION_TYPE_CIPHERS && "Wrong selection type");

	int count = sshconfig_selection_type_get_option_count(SSH_SELECTION_TYPE_CIPHER);
	assert(count == 3 && "Wrong selection type count");

	const SshRustStr * strs = sshconfig_selection_type_get_option_strs(SSH_SELECTION_TYPE_CIPHER);
	assert(strcmp(strs[0].string, "blowfish") == 0 && strcmp(strs[1].string, "3des") == 0 && strcmp(strs[2].string, "des") == 0);
}

