#include "sshconfig.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>

void test_bool_opt() {
	bool input = true;
	SshOpt * opt = sshconfig_bool_opt_create(input);
	bool output = sshconfig_bool_from_opt(opt);
	assert(input == output && "Bool Opt: Input and Output differ");
	sshconfig_opt_free(opt);
}

void test_int_opt() {
	int input = 42;
	SshOpt * opt = sshconfig_int_opt_create(input);
	int output = sshconfig_int_from_opt(opt);
	assert(input == output && "Int Opt: Input and Output differ");
	sshconfig_opt_free(opt);
}

void test_string_opt() {
	char * input = "Input String";
	SshOpt * opt = sshconfig_string_opt_create(input);
	const char * output = sshconfig_string_from_opt(opt);
	assert(strcmp(input, output) == 0 && "Bool Opt: Input and Output differ");
	sshconfig_opt_free(opt);
}

void test_forward_opt() {
	char * input_from = "From";
	char * input_to = "To";
	SshOpt * opt = sshconfig_forward_opt_create(input_from, input_to);
	const char ** output = malloc(2 * sizeof(char*));
	sshconfig_forward_from_opt(opt, output);
	assert(strcmp(input_from, output[0]) == 0 && strcmp(input_to, output[1]) == 0 && "Forward Opt: Input and Output differ");
	free(output);
	sshconfig_opt_free(opt);
}

void test_selection_opt() {
	SshSelectionType input_type = SSH_SELECTION_TYPE_LOG_LEVEL;
	SshLogLevel input_option = SSH_LOG_LEVEL_DEBUG;
	SshOpt * opt = sshconfig_selection_opt_create(input_type, input_option);
	SshSelectionType output_type = sshconfig_selection_type_from_opt(opt);
	SshLogLevel output_option = sshconfig_selection_option_from_opt(opt);
	assert(input_type == output_type && "Selection Opt: Input and Output types differ");
	assert(input_option == output_option && "Selection Opt: Input and Output values differ");
	sshconfig_opt_free(opt);
}

void test_selection_list_opt() {
	SshSelectionType input_type = SSH_SELECTION_TYPE_MACS;
	uint8_t * input = malloc(2 * sizeof(uint8_t*));
	input[0] = SSH_MACS_H_MAC_MD5;
	input[1] = SSH_MACS_H_MAC_SHA1_96;
	SshOpt * opt = sshconfig_selection_list_opt_create(input_type, input, 2);
	int count = sshconfig_selection_option_count_from_opt(opt);
	uint8_t * output = malloc(count * sizeof(uint8_t*));
	sshconfig_selection_option_list_from_opt(opt, output);
	SshSelectionType output_type = sshconfig_selection_type_from_opt(opt);

	assert(count == 2 && "Selection List Opt: Input and Output list lengths differ");
	assert(input[0] == output[0] && "Selection List Opt: Input and Output differ");
	assert(input[1] == output[1] && "Selection List Opt: Input and Output differ");
	assert(input_type == output_type && "Selection List Opt: Input and Output types differ");

	sshconfig_opt_free(opt);
	free(input);
	free(output);
}

int main() {
	test_bool_opt();
	test_int_opt();
	test_string_opt();
	test_forward_opt();
	test_selection_opt();
	test_selection_list_opt();
}
