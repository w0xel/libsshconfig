#include "sshconfig.h"
#include <assert.h>

int main() {
	SshConfigFile * file = sshconfig_file_open("example_config");
	assert(file && "Could not open file");
	sshconfig_file_close(file);
}
