#include "sshconfig.h"
#include <assert.h>
#include <string.h>

void test_config_1(const SshConfig * config) {
	assert(config && "Parse did not yield results");

	const char * name = sshconfig_get_name(config);
	assert(strcmp(name, "ExampleConfig1") == 0 && "Parsed name is wrong");

	int option_count = sshconfig_get_option_count(config);
	assert(option_count == 3 && "Parsed option count is wrong");

	for (int i = 0; i < 3; i++) {
		const SshConfigOption * option = sshconfig_get_option_ref(config, i);
		assert(option && "Not enough options in config after parse");
		
		SshKey type = sshconfig_option_get_key(option);
		switch (i) {
			case 0:
				assert(type == SSH_KEY_HOSTNAME && "Wrong option type after parse");
				const char * hostname = sshconfig_option_as_str(option);

				assert(strcmp(hostname, "host.name") == 0 && "Wrong Hostname after parse");
				break;
			case 1:
				assert(type == SSH_KEY_PORT && "Wrong option type after parse");
				int port = sshconfig_option_as_int(option);

				assert(port == 3748 && "Wrong port after parse");
				break;
			case 2:
				assert(type == SSH_KEY_CIPHER && "Wrong option type after parse");
				SshSelectionType sel_type = sshconfig_key_get_selection_type(type);

				assert(sel_type == SSH_SELECTION_TYPE_CIPHER && "Wrong selection type after parse");
				SshCipher cipher = sshconfig_option_as_selection(option);

				assert(cipher == SSH_CIPHER_DES && "Wrong cipher after parse");
				break;
		}
	}
}

void test_config_2(const SshConfig * config) {
	assert(config && "Parse did not yield results");

	const char * name = sshconfig_get_name(config);
	assert(strcmp(name, "ExampleConfig2") == 0 && "Parsed name is wrong");

	int option_count = sshconfig_get_option_count(config);
	assert(option_count == 3 && "Parsed option count is wrong");

	for (int i = 0; i < 3; i++) {
		const SshConfigOption * option = sshconfig_get_option_ref(config, i);
		assert(option && "Not enough options in config after parse");
		
		SshKey type = sshconfig_option_get_key(option);
		switch (i) {
			case 0:
				assert(type == SSH_KEY_MACS && "Wrong option type after parse");
				int count = sshconfig_option_get_selection_list_length(option);
				
				assert(count == 2 && "Wrong MAC count after parse");

				uint8_t * options = malloc (count * sizeof(uint8_t)); 
				int size = sshconfig_option_as_selection_list(option, options);
				assert(options[0] == SSH_MACS_H_MAC_SHA1 && options[1] == SSH_MACS_H_MAC_MD5_96 && "Wrong Macs after parse");
				free(options);

				break;
			case 1:
				assert(type == SSH_KEY_LOCAL_FORWARD && "Wrong option type after parse");

				const SshRustStr * pair = sshconfig_option_as_forward(option);
				assert(strcmp(pair[0].string, "7754") == 0 && strcmp(pair[1].string, "localhost:342") == 0 && "Wrong forward options after parse");

				break;
			case 2:
				assert(type == SSH_KEY_FORWARD_X11 && "Wrong option type after parse");

				bool b = sshconfig_option_as_bool(option);

				assert(b && "Wrong bool after parse");
				break;
		}
		
	}
}

int main() {
	SshConfigFile * file = sshconfig_file_open("example_config");
	assert(file && "Could not open file");
	
	bool result = sshconfig_file_parse(file);
	assert(result && "Could not parse file");


	const SshConfig * config = sshconfig_file_get_config_ref(file, 0);
	test_config_1(config);
	const SshConfig * byName = sshconfig_file_get_config_ref_by_name(file, "ExampleConfig1");
	assert(config == byName && "Get Config by name does not work.");
	
	config = sshconfig_file_get_config_ref(file, 1);
	test_config_2(config);

	const SshConfig * config2 = sshconfig_file_get_config_ref(file, 2);
	assert(!config2 && "Too many configs after parse");

	sshconfig_file_remove_config(file, config);
	config = sshconfig_file_get_config_ref(file, 1);
	assert(!config && "Could not remove config");


	sshconfig_file_close(file);
}
