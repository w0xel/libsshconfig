#include "libsshconfig.h"
#include <stdio.h>

int main() {
	SshConfig * conf = sshconfig_create("asdf");
	printf("%s\n", sshconfig_get_name(conf));

	int c = sshconfig_get_selection_option_count(SSH_SELECTION_TYPE_LOG_LEVEL);
	printf("Count: %d\n", c);

	const char ** strs = malloc(c * sizeof(char*));
	sshconfig_get_selection_option_strs(SSH_SELECTION_TYPE_LOG_LEVEL, strs);

	for (int i = 0; i < c; i++) {
		printf("Str %d: %s\n",i, strs[i]);
	}

	SshOpt * opt = sshconfig_selection_opt_create(SSH_SELECTION_TYPE_LOG_LEVEL, SSH_LOG_LEVEL_DEBUG);
	sshconfig_add_opt(conf, SSH_CONFIG_OPTION_TYPE_LOG_LEVEL, opt);
	sshconfig_opt_free(opt);

	const SshConfigOption * conf_option = sshconfig_get_option_ref_for_type(conf, SSH_CONFIG_OPTION_TYPE_LOG_LEVEL);
	opt = sshconfig_opt_create(conf_option);
	uint8_t sel = sshconfig_selection_option_from_opt(opt);
	printf("Sel: %d\n", sel);
	sshconfig_opt_free(opt);
	sshconfig_free(conf);

	conf = sshconfig_create("w0xel");
	c = sshconfig_config_option_type_count();
	for (int i = 0; i < c; i++) {
		SshOptType type = sshconfig_get_opt_type(i);
		switch (type) {
			case SSH_OPT_TYPE_INT:
				opt = sshconfig_int_opt_create(i);
				break;
			case SSH_OPT_TYPE_BOOL:
				opt = sshconfig_bool_opt_create(i % 2);
				break;
			case SSH_OPT_TYPE_FORWARD: {
				char * from = "from:9898";
				char * to = "to:234";
				opt = sshconfig_forward_opt_create(from, to);
			}
				break;
			case SSH_OPT_TYPE_STRING: {
				char * string = "string";
				opt = sshconfig_string_opt_create(string);
			}
				break;
			case SSH_OPT_TYPE_SELECTION: {
				SshSelectionType type = sshconfig_get_selection_type(i);
				int count = sshconfig_get_selection_option_count(type);
				opt = sshconfig_selection_opt_create(type, count - 1);
			}
				break;
			case SSH_OPT_TYPE_SELECTION_LIST: {
				SshSelectionType type = sshconfig_get_selection_type(i);
				int count = sshconfig_get_selection_option_count(type);
				uint8_t * arr = malloc(count * sizeof(uint8_t*));
				for (int k = 0; k < count; k++) {
					arr[k] = k;					
				}
				opt = sshconfig_selection_list_opt_create(type, arr, count);
				free(arr);
			}
				break;
			default:
				return 1;
		}
		sshconfig_add_opt(conf, i, opt);
		sshconfig_opt_free(opt);
	}

	char * buf_name = malloc(40 * sizeof(char));
	char * buf_set = malloc(400 * sizeof(char));
	for (int i = 0; i < c; i++) {
		conf_option = sshconfig_get_option_ref_for_type(conf, i);
		opt = sshconfig_opt_create(conf_option);
		SshOptType type = sshconfig_opt_get_type(opt);
		sshconfig_config_option_type_to_str(i, buf_name);
		sshconfig_opt_to_str(opt, buf_set);
		printf("%s %s\n", buf_name, buf_set);
		
		sshconfig_opt_free(opt);
	}

	sshconfig_free(conf);
	free(strs);

	SshConfigFile * file = sshconfig_file_open("/home/sebastian/.ssh/config");
	if (!file) {
		const char * err = sshconfig_get_error();
		printf("SSHConfig Error: %s\n", err);
		free(buf_name);
		free(buf_set);
		return 1;
	}
	sshconfig_file_parse(file);
	const SshConfig * config = sshconfig_file_get_config_ref(file, 0);
	const SshConfigOption * option;

	SshConfigFile * file_new = sshconfig_file_create_or_open("/home/sebastian/test_config");
	if (!file_new) {
		const char * err = sshconfig_get_error();
		printf("SSHConfig Error: %s\n", err);
		free(buf_name);
		free(buf_set);
		sshconfig_file_close(file);
		return 1;
	}

	int i = 1;
	while (config) {
		printf("\nHost %s\n", sshconfig_get_name(config));
		option = sshconfig_get_option_ref(config, 0);
		sshconfig_file_add_config(file_new, config);
		
		int k = 1;
		while (option) {
			SshConfigOptionType type = sshconfig_option_get_type(option);
			opt = sshconfig_opt_create(option);
			sshconfig_config_option_type_to_str(type, buf_name);
			sshconfig_opt_to_str(opt, buf_set);
			if (type == SSH_CONFIG_OPTION_TYPE_COMMENT) {
				printf("	comment -> %s%s\n", buf_name, buf_set);
			} else if (type == SSH_CONFIG_OPTION_TYPE_INVALID) {
				printf("	invalid -> %s\n", buf_set);
			} else {
				printf("	%s %s\n", buf_name, buf_set);
			}
			sshconfig_opt_free(opt);
			
			option = sshconfig_get_option_ref(config, k);
			k++;
		}
		config = sshconfig_file_get_config_ref(file, i);
		i++;
	}

	printf("Success: %d\n", sshconfig_file_write(file_new));
	sshconfig_file_close(file_new);
	
	free(buf_name);
	free(buf_set);
	sshconfig_file_close(file);
}
