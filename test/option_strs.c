#include "sshconfig.h"
#include <assert.h>
#include <stdio.h>

int main() {
	const SshRustStr * strs = sshconfig_selection_type_get_option_strs(SSH_SELECTION_TYPE_CIPHER);
	for (int i = 0; i < sshconfig_selection_type_get_option_count(SSH_SELECTION_TYPE_CIPHER); i++) {
		printf("%s\n", strs[i].string);
	}

	printf("%s\n", sshconfig_key_to_str(SSH_KEY_USER));
}
