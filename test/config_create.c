#include "sshconfig.h"
#include <assert.h>
#include <string.h>

int main() {
	SshConfig * conf = sshconfig_create("Name");
	assert(conf && "Could not create config");
	const char * name = sshconfig_get_name(conf);
	assert(strcmp(name, "Name") == 0 && "Config set name did not work");

	sshconfig_set_name(conf, "OtherName");

	name = sshconfig_get_name(conf);
	assert(strcmp(name, "OtherName") == 0 && "Config set name did not work");
	sshconfig_free(conf);
}
