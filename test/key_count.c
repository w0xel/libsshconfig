#include "sshconfig.h"
#include <assert.h>

int main() {
	int count = sshconfig_key_count();

	// + 2 for comment and invalid options
	assert (count == 69 + 2 && "Invalid option count!");
}
