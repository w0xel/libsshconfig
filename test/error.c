#include "sshconfig.h"
#include <string.h>
#include <stdio.h>
#include <assert.h>

int main() {
	sshconfig_file_open("invalid path");

	const char * err = sshconfig_get_error();
	printf("Err: %s\n", err);

	assert(strlen(err) > 1 && "Error not working");
}
