#include "sshconfig.h"
#include <assert.h>
#include <string.h>

int main() {
	SshConfig * conf = sshconfig_create("Name");

	sshconfig_set_string(conf, SSH_KEY_IDENTITY_FILE, "file1.asd");
	sshconfig_set_int(conf, SSH_KEY_PORT, 1231);
	sshconfig_set_string(conf, SSH_KEY_IDENTITY_FILE, "file2.asd");

	int count = sshconfig_get_option_count_for_key(conf, SSH_KEY_IDENTITY_FILE);
	assert(count == 2 && "Wrong option count after double insert");

	const SshConfigOption * ref = sshconfig_get_option_ref_for_key(conf, SSH_KEY_IDENTITY_FILE, 0);
	const char * str = sshconfig_option_as_str(ref);

	assert(strcmp(str, "file1.asd") == 0 && "Wrong option 1");

	ref = sshconfig_get_option_ref_for_key(conf, SSH_KEY_IDENTITY_FILE, 1);
	str = sshconfig_option_as_str(ref);
	
	assert(strcmp(str, "file2.asd") == 0 && "Wrong option 2");

	sshconfig_free(conf);
}
