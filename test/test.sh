#!/bin/bash

function get_error {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $1" >&2
    fi
    return $status
}

export SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

function test_all {
	pushd "$SCRIPTPATH" > /dev/null
	pushd .. > /dev/null
	cbindgen -c cbindgen.toml -o sshconfig.h . > /dev/null
	pushd test > /dev/null
	make clean > /dev/null
	RES=$(get_error make all)
	RET=$?
	
	if [[ $RET -ne 0 ]]; then
		popd > /dev/null
		echo "Error during make:"
		echo "$RES"
		return 1
	fi
	
	for exe in build/*; do
		echo -n "Running test: $(basename $exe) ..."
		RES=$(env LD_LIBRARY_PATH="$PWD/../target/debug" env RUST_BACKTRACE=1 ./$exe 2>&1)
		RET=$?
		if [[ $RET -ne 0 ]]; then
			echo " FAILED!"
			popd > /dev/null
			echo ""
			echo "Error:"
			echo "$RES"
			echo ""
			
			return 2
		fi
		echo " passed!"
	done
	
	popd > /dev/null
	return 0
}

test_all
