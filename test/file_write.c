#include "sshconfig.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>

void compare_files() {
	FILE *fp1, *fp2;
	int ch1, ch2;
	
	char * fname1 = "example_config";
	char * fname2 = "example_config2";
	
	fp1 = fopen(fname1, "r");
	fp2 = fopen(fname2, "r");

	assert(fp1 && "Could not open file");
	assert(fp2 && "New File does not exist!");
	
	ch1 = getc(fp1);
	ch2 = getc(fp2);
	
	while ((ch1 != EOF) && (ch2 != EOF) && (ch1 == ch2)) {
		ch1 = getc(fp1);
		ch2 = getc(fp2);
	}
	
	assert(ch1 == ch2 && "Files are not identical");
	
	fclose(fp1);
	fclose(fp2);
}

int main() {
	SshConfigFile * file = sshconfig_file_create_or_open("example_config2");
	assert(file && "Could not create file");

	SshConfig * config = sshconfig_file_add_new_config(file, "ExampleConfig1");
	sshconfig_set_string(config, SSH_KEY_HOSTNAME,"host.name");
	sshconfig_set_int(config, SSH_KEY_PORT, 3748);
	sshconfig_set_selection(config, SSH_KEY_CIPHER, SSH_CIPHER_DES);
	
	config = sshconfig_create("ExampleConfig2");
	uint8_t options[2] = { SSH_MACS_H_MAC_SHA1, SSH_MACS_H_MAC_MD5_96 };
	sshconfig_set_selection_list(config, SSH_KEY_MACS, options, 2);
	sshconfig_set_forward(config, SSH_KEY_LOCAL_FORWARD, "7754", "localhost:342");
	sshconfig_set_bool(config, SSH_KEY_FORWARD_X11, true);
	sshconfig_set_int(config, SSH_KEY_PORT, 123);
	assert(sshconfig_unset(config, SSH_KEY_PORT) && "Unset does not work");

	sshconfig_set_string(config, SSH_KEY_HOSTNAME,"host.name");
	const SshConfigOption * option = sshconfig_get_option_ref_by_key(config, SSH_KEY_HOSTNAME);
	sshconfig_remove_option(config, option);
	sshconfig_file_add_clone_config(file, config);
	sshconfig_free(config);

	sshconfig_file_write(file);
	sshconfig_file_close(file);

	compare_files();
}
