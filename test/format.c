#include "sshconfig.h"
#include <string.h>
#include <assert.h>

int main() {
	const char * str = sshconfig_key_to_str(SSH_KEY_CIPHER);
	assert(strcmp(str, "Cipher") == 0);

	str = sshconfig_selection_option_as_str(SSH_SELECTION_TYPE_CIPHER, 1);
	assert(strcmp(str, "3des") == 0);
}
