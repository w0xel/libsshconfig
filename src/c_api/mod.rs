/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
use std::ffi::{CStr, CString};
use libc::{c_char};
use std::cell::RefCell;

mod util;

use config::*;
use parser::ConfigFile;

/*
 * ERROR HANDLING
 */
fn internal_error(err: Option<String>) -> *const c_char {
    thread_local!{static ERR: RefCell<Option<CString>> = RefCell::new(None);}

    if let Some(s) = err {
        ERR.with(|f| {
            *f.borrow_mut() = Some(CString::new(s).unwrap());
        });
        0 as *const c_char
    } else {
        let mut ret = 0 as *const c_char;
        ERR.with(|f| {
            if let Some(ref err) = *f.borrow() {
                ret = err.as_ptr();
            }
        });
        ret
    }
}

#[no_mangle]
pub extern fn sshconfig_get_error() -> *const c_char {
    internal_error(None)
}

/*
 * CONFIG FILE FUNCTIONS
 */
#[no_mangle]
pub extern fn sshconfig_file_open(path: *const c_char) -> *mut ConfigFile {
    let path = util::c_to_string(path);
    match ConfigFile::open(path) {
        Ok(file) => {
            let f = Box::new(file);
            Box::into_raw(f)
        },
        Err(why) => {
            internal_error(Some(why.to_string()));
            0 as *mut ConfigFile
        },
    }
}

#[no_mangle]
pub extern fn sshconfig_file_create_or_open(path: *const c_char) -> *mut ConfigFile {
    let path = util::c_to_string(path);
    match ConfigFile::create_or_open(path) {
        Ok(file) => {
            let f = Box::new(file);
            Box::into_raw(f)
        },
        Err(why) => {
            internal_error(Some(why.to_string()));
            0 as *mut ConfigFile
        },
    }
}

#[no_mangle]
pub extern fn sshconfig_file_close(file: *mut ConfigFile) {
    assert!(!file.is_null());
    unsafe { Box::from_raw(file) };
}

#[no_mangle]
pub extern fn sshconfig_file_parse(file: *mut ConfigFile) -> bool {
    assert!(!file.is_null());
    let file = unsafe { &mut *file };
    if let Err(why) = file.parse() {
        internal_error(Some(why.to_string()));
        return false;
    }
    return true;
}

#[no_mangle]
pub extern fn sshconfig_file_write(file: *mut ConfigFile) -> bool {
    assert!(!file.is_null());
    let file = unsafe { &mut *file };
    match file.write() {
        Ok(_) => true,
        Err(why) => {
            internal_error(Some(why.to_string()));
            false
        },
    }
}

#[no_mangle]
pub extern fn sshconfig_file_get_config_ref(file: *const ConfigFile, index: u32) -> *const Config {
    assert!(!file.is_null());
    let file = unsafe { &*file };

    match file.get_config_by_index(index as usize) {
        Some(conf) => conf as *const Config,
        None => 0 as *const Config,
    }
}

#[no_mangle]
pub extern fn sshconfig_file_get_config_ref_by_name(file: *const ConfigFile, name: *const c_char) -> *const Config {
    assert!(!file.is_null());
    assert!(!name.is_null());
    let file = unsafe { &*file };

    let str_name = util::c_to_string(name);
    
    match file.get_config_by_name(str_name) {
        Some(conf) => conf as *const Config,
        None => 0 as *const Config,
    }
}

#[no_mangle]
pub extern fn sshconfig_file_remove_config(file: *mut ConfigFile, config: *const Config) -> bool {
    assert!(!file.is_null());
    assert!(!config.is_null());
    
    let config = unsafe { & *config };
    let file = unsafe { &mut *file };

    file.remove_config(config)
}

#[no_mangle]
pub extern fn sshconfig_file_add_clone_config(file: *mut ConfigFile, config: *const Config) -> *mut Config {
    assert!(!file.is_null());
    assert!(!config.is_null());
    let config = unsafe { & *config };
    let file = unsafe { &mut *file };
    let config = config.clone();
    let conf = file.add_config(config);
    let ptr = conf as *mut Config;
    ptr
}

#[no_mangle]
pub extern fn sshconfig_file_add_new_config(file: *mut ConfigFile, name: *const c_char) -> *mut Config {
    assert!(!file.is_null());
    assert!(!name.is_null());
    let file = unsafe { &mut *file };
    let str_name = util::c_to_string(name);
    let config = Config::new(str_name);
    let conf = file.add_config(config);
    let ptr = conf as *mut Config;
    ptr
}

/*
 * SSH CONFIG FUNCTIONS
 */
#[no_mangle]
pub extern fn sshconfig_create(name: *const c_char) -> *mut Config {
    assert!(!name.is_null());
    let str_name = util::c_to_string(name);
    let boxed = Box::new(Config::new(str_name));
    Box::into_raw(boxed)
}

#[no_mangle]
pub extern fn sshconfig_free(config: *mut Config) {
    assert!(!config.is_null());
    unsafe { Box::from_raw(config) };
}

#[no_mangle]
pub extern fn sshconfig_set_name(config: *mut Config, name: *const c_char) {
    assert!(!name.is_null());
    let mut_config = unsafe { &mut *config };
    mut_config.set_name(util::c_to_string(name));
}

#[no_mangle]
pub extern fn sshconfig_get_name(config: *const Config) -> *const c_char {
    assert!(!config.is_null());
    let conf = unsafe { & *config };
    conf.get_name_c().as_ptr()
    
}

#[no_mangle]
pub extern fn sshconfig_get_option_count(config: *const Config) -> u32 {
    assert!(!config.is_null());
    let conf = unsafe { & *config };
    conf.get_option_count() as u32
}

#[no_mangle]
pub extern fn sshconfig_key_count() -> u32 {
    ConfigOptionType::option_count()
}

#[no_mangle]
pub extern fn sshconfig_key_get_type(ty: ConfigOptionType) -> OptType {
    ConfigOptionType::get_opt_type(&ty)
}

#[no_mangle]
pub extern fn sshconfig_set_bool(config: *mut Config, ty: ConfigOptionType, value: bool) {
    assert!(!config.is_null());
    assert!(ConfigOptionType::get_opt_type(&ty) == OptType::Bool);
    let conf = unsafe { &mut *config };
    let option = ConfigOption::from_bool(ty, value).unwrap();
    conf.add_option(option);
}

#[no_mangle]
pub extern fn sshconfig_set_int(config: *mut Config, ty: ConfigOptionType, value: u32) {
    assert!(!config.is_null());
    assert!(ConfigOptionType::get_opt_type(&ty) == OptType::Int);
    let conf = unsafe { &mut *config };
    let option = ConfigOption::from_int(ty, value).unwrap();
    conf.add_option(option);
}

#[no_mangle]
pub extern fn sshconfig_set_string(config: *mut Config, ty: ConfigOptionType, value: *const c_char) {
    assert!(!config.is_null());
    assert!(!value.is_null());
    assert!(ConfigOptionType::get_opt_type(&ty) == OptType::String);
    let conf = unsafe { &mut *config };
    let c_str = unsafe { CStr::from_ptr(value) };
    let option = ConfigOption::from_string(ty, c_str.to_str().unwrap()).unwrap();
    conf.add_option(option);
}

#[no_mangle]
pub extern fn sshconfig_set_forward(config: *mut Config, ty: ConfigOptionType, from: *const c_char, to: *const c_char) {
    assert!(!config.is_null());
    assert!(!from.is_null());
    assert!(!to.is_null());
    assert!(ConfigOptionType::get_opt_type(&ty) == OptType::Forward);
    let conf = unsafe { &mut *config };
    let c_str_from = unsafe { CStr::from_ptr(from) };
    let c_str_to = unsafe { CStr::from_ptr(to) };
    let option = ConfigOption::from_forward(ty, Forward{from: c_str_from.to_owned(), to: c_str_to.to_owned()}).unwrap();
    conf.add_option(option);
}

#[no_mangle]
pub extern fn sshconfig_set_selection(config: *mut Config, ty: ConfigOptionType, option: u8) {
    assert!(!config.is_null());
    assert!(ConfigOptionType::get_opt_type(&ty) == OptType::Selection);
    let conf = unsafe { &mut *config };
    let option = ConfigOption::from_selection(ty, option).unwrap();
    conf.add_option(option);
}

#[no_mangle]
pub extern fn sshconfig_set_selection_list(config: *mut Config, ty: ConfigOptionType, options: *const u8, count: u8) {
    assert!(!config.is_null());
    assert!(ConfigOptionType::get_opt_type(&ty) == OptType::SelectionList);
    let conf = unsafe { &mut *config };
    let mut vec = Vec::<u8>::new();
    let mut opt_count = count;
    while opt_count > 0 {
        vec.push(unsafe { options.offset((count - opt_count) as isize).read() });
        opt_count -= 1;
    }
    let option = ConfigOption::from_selection_list(ty, vec).unwrap();
    conf.add_option(option);
}

#[no_mangle]
pub extern fn sshconfig_unset(config: *mut Config, ty: ConfigOptionType) -> bool {
    assert!(!config.is_null());
    let conf = unsafe { &mut *config };
    
    conf.remove_option_with_type(ty)
}

#[no_mangle]
pub extern fn sshconfig_remove_option(config: *mut Config, option: *const ConfigOption) {
    assert!(!config.is_null());
    assert!(!option.is_null());
    
    let config = unsafe { &mut *config };
    let option = unsafe { & *option };

    config.remove_option(option);
}

#[no_mangle]
pub extern fn sshconfig_get_option_ref(config: *const Config, i: u32) -> *const ConfigOption {
    assert!(!config.is_null());
    let conf = unsafe { & *config };

    let vec = conf.get_options();
    let i = i as usize;
    if vec.len() <= i {
        0 as *const ConfigOption
    } else {
        &vec[i] as *const ConfigOption
    }
}

#[no_mangle]
pub extern fn sshconfig_get_option_ref_by_key(config: *const Config, option_type: ConfigOptionType) -> *const ConfigOption {
    assert!(!config.is_null());
    let conf = unsafe { & *config };
    let vec = conf.get_options();

    for x in vec.iter() {
        if x.get_variant() == option_type {
            return x as *const ConfigOption;
        }
    }
    return 0 as *const ConfigOption;
}

#[no_mangle]
pub extern fn sshconfig_get_option_count_for_key(config: *const Config, key: ConfigOptionType) -> u32 {
    assert!(!config.is_null());
    let conf = unsafe { & *config };

    let options = conf.get_options();
    let mut i = 0;
    for x in options.iter() {
        if x.get_variant() == key {
            i += 1;
        }
    }
    i
}

#[no_mangle]
pub extern fn sshconfig_get_option_ref_for_key(config: *mut Config, key: ConfigOptionType, index: u32) -> *const ConfigOption {
    assert!(!config.is_null());
    let conf = unsafe { &mut *config };
    
    let vec = conf.get_mut_options();
    let mut i = 0;
    for x in vec.iter_mut() {
        if x.get_variant() == key {
            if i == index {
                return x as *mut ConfigOption;
            }
            i += 1;
        }
    }
    return 0 as *mut ConfigOption
}

#[no_mangle]
pub extern fn sshconfig_key_get_selection_type(option_type: ConfigOptionType) -> SelectionType {
    assert!({
        let opt_type = option_type.get_opt_type();
        opt_type == OptType::Selection || opt_type == OptType::SelectionList
    });
    option_type.get_selection_type().unwrap()
}
#[no_mangle]
pub extern fn sshconfig_option_get_key(option: *const ConfigOption) -> ConfigOptionType {
    assert!(!option.is_null());
    let option = unsafe { & *option };
    option.get_variant()
}

#[no_mangle]
pub extern fn sshconfig_selection_type_get_option_count(selection_type: SelectionType) -> i32 {
    Selection::option_count(selection_type)
}

#[repr(C)]
pub struct RustStr {
    string: *const c_char,
    length: usize,
}

#[no_mangle]
pub extern fn sshconfig_selection_type_get_option_strs(opt: SelectionType) -> *const RustStr {
    Selection::cstr_slice(opt).as_ptr() as *const _ as *const RustStr
}

#[no_mangle]
pub extern fn sshconfig_key_to_str(ty: ConfigOptionType) -> *const c_char {
    ty.as_str().as_ptr()
}

#[no_mangle]
pub extern fn sshconfig_selection_option_as_str(selection: SelectionType, option: u8) -> *const c_char {
    match Selection::from_int(selection, option) {
        Some(x) => x.as_cstr().as_ptr(),
        None => 0 as *const c_char,
    }
}

#[no_mangle]
pub extern fn sshconfig_option_as_int(option: *const ConfigOption) -> u32 {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::Int);
    option.get_int().unwrap()
}

#[no_mangle]
pub extern fn sshconfig_option_as_bool(option: *const ConfigOption) -> bool {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::Bool);
    option.get_bool().unwrap()
}

#[no_mangle]
pub extern fn sshconfig_option_as_str(option: *const ConfigOption) -> *const c_char {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::String);
    option.get_string().unwrap().as_ptr()
}

#[no_mangle]
pub extern fn sshconfig_option_as_forward(option: *const ConfigOption) -> *const RustStr {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::Forward);
    option.get_forward().unwrap() as *const _ as *const RustStr
}

#[no_mangle]
pub extern fn sshconfig_option_as_selection(option: *const ConfigOption) -> u8 {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::Selection);
    option.get_selection().unwrap()
}

#[no_mangle]
pub extern fn sshconfig_option_as_selection_list(option: *const ConfigOption, buf: *mut u8) -> u32 {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::SelectionList);
    let vec = option.get_selection_list().unwrap();
    let mut i = 0;
    for x in vec.iter() {
        unsafe {buf.offset(i).write(*x);}
        i += 1;
    }
    i as u32
}

#[no_mangle]
pub extern fn sshconfig_option_get_selection_list_length(option: *const ConfigOption) -> u32 {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    assert!(option.get_opt_type() == OptType::SelectionList);
    option.get_selection_list().unwrap().len() as u32
}


#[no_mangle]
pub extern fn sshconfig_option_get_type(option: *const ConfigOption) -> OptType {
    assert!(!option.is_null());
    let option = unsafe { & *option };

    option.get_opt_type()
}
