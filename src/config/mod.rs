/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
use std::ffi::{CString};

pub mod option;
pub use self::option::*;

pub mod opt;
pub use self::opt::*;
pub use self::opt::selection::*;

#[derive(Clone)]
pub struct Config {
    name:	CString,
    config: Vec<ConfigOption>,
}

impl Config {
    pub fn new(name: String) -> Self {
        Config {name: CString::new(name).unwrap(), config: Vec::<ConfigOption>::new()}
    }
    
    pub fn set_name(&mut self, name: String) {
        self.name = CString::new(name).unwrap();
    }

    pub fn get_name(& self) -> String {
        return String::from(self.name.to_str().unwrap());
    }

    pub fn get_name_c(& self) -> &CString {
        return &self.name;
    }

    pub fn add_option(&mut self, opt: ConfigOption) {
        let ty = opt.get_variant();
        if ty == ConfigOptionType::IdentityFile ||
            ty == ConfigOptionType::SendEnv ||
            ty == ConfigOptionType::RemoteForward ||
            ty == ConfigOptionType::LocalForward ||
            ty == ConfigOptionType::DynamicForward
        {
            for x in self.config.iter() {
                if x == &opt {
                    return;
                }
            }
        } else {
            for x in self.config.iter() {
                if x.get_variant() == opt.get_variant() {
                    return;
                }
            }
        }
        self.config.push(opt);
    }

    pub fn remove_option(&mut self, opt: &ConfigOption) {

        let index = self.config.iter().position(|x| x == opt);
        if let Some(i) = index {
            self.config.remove(i);
        }
    }

    pub fn remove_option_with_type(&mut self, opt_type: ConfigOptionType) -> bool {
        let mut index = self.config.iter().position(|x| x.is_variant(opt_type));
        if let None = index {
            return false;
        }
        while let Some(i) = index {
            self.config.remove(i);
            index = self.config.iter().position(|x| x.is_variant(opt_type));
        }
        true
    }

    pub fn get_option(&self, opt_type: ConfigOptionType) -> Option<&ConfigOption> {
        let index = self.config.iter().position(|x| x.is_variant(opt_type) );
        if let Some(i) = index {
            return Some(&self.config[i]);
        }
        return None;
    }

    pub fn get_option_count(&self) -> usize {
        self.config.len()
    }

    pub fn get_options(&self) -> &Vec<ConfigOption> {
        &self.config
    }
    
    pub fn get_mut_options(&mut self) -> &mut Vec<ConfigOption> {
        &mut self.config
    }

}

