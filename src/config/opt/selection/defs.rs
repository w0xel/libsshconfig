/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#[path = "ident_count.rs"]
#[macro_use]
mod ident_count;

use std::ffi::CStr;
use std::fmt::{Debug, Formatter, Error, Display};
use std::slice;

pub trait SelectionEnum {
    fn as_cstr(&self) -> &'static CStr;
}

macro_rules! selection_enum {
    ($name:ident, $($str:expr => $var:ident,)*) => {
        #[derive(PartialEq, Copy, Clone)]
        #[repr(C)]
        pub enum $name {
            $($var,)*
        }

        impl SelectionEnum for $name {
            fn as_cstr(&self) -> &'static CStr {
                $name::cstr_slice()[*self as usize]
            }
        }

        impl Debug for $name {
            fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
                f.write_str(self.as_str().as_str())
            }
        }

        impl Display for $name {
            fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
                f.write_str(&self.as_str())
            }
        }

        impl $name {
            pub fn cstr_slice() -> &'static [&'static CStr] {
                static C_STRS: [&'static str; ident_count!{$($var),*}] = [$(concat!($str, "\0")),*];

                // Manual conversion from str to cstr to keep it const and c-compatible
                unsafe {slice::from_raw_parts(&C_STRS as *const _ as *const &CStr, ident_count!{$($var),*})}
            }

            pub fn from_int(i: u8) -> Option<$name> {
                match i {
                    $(i if i == $name::$var as u8 => Some($name::$var),)*
                    _ => None,
                }
            }

            pub fn option_count() -> i32 {
                ident_count!($($var),*)
            }

            pub fn all_const_cstrs() -> Vec<&'static CStr> {
                let mut vec = Vec::<&'static CStr>::new();
                $(vec.push($name::$var.as_cstr());)*
                return vec;
            }

            pub fn all_strings() -> Vec<String> {
                let mut vec = Vec::<String>::new();
                $(vec.push($name::$var.to_string());)*
                return vec;
            }

            fn as_str(&self) -> String {
                self.as_cstr().to_str().unwrap().to_owned()
            }
        }
    }
}

selection_enum! {
    Cipher,
    "blowfish" => Blowfish,
    "3des" => TrippleDes,
    "des" => Des,
}

selection_enum! {
    Ciphers,
    "aes128-ctr" => Aes128Ctr,
    "aes192-ctr" => Aes192Ctr,
    "aes256-ctr" => Aes256Ctr,
    "arcfour-256" => Arcfour256,
    "arcfour-128" => Arcfour128,
    "aes128-cbc" => Aes128Cbc,
    "3des-cbc" => TrippledesCbc,
    "blowfish-cbc" => BlowfishCbc,
    "cast128-cbc" => Cast128Cbc,
    "aes192-cbc" => Aes192Cbc,
    "aes256-cbc" => Aes256Cbc,
    "arcfour" => Arcfour,
}

selection_enum! {
    AddressFamily,
    "any" => Any,
    "inet" => IpV4,
    "inet6" => IpV6,
}

selection_enum! {
    ControlMaster,
    "yes" => Yes,
    "no" => No,
    "ask" => Ask,
    "auto" => Auto,
    "autoask" => AutoAsk,
}

selection_enum! {
    KbdInteractiveDevices,
    "bsdauth" => BsdAuth,
    "pam" => Pam,
    "skey" => SKey,
}

selection_enum! {
    LogLevel,
    "QUIET" => Quiet,
    "FATAL" => Fatal,
    "ERROR" => Error,
    "INFO" => Info,
    "VERBOSE" => Verbose,
    "DEBUG" => Debug,
    "DEBUG1" => Debug1,
    "DEBUG2" => Debug2,
    "DEBUG3" => Debug3,
}

selection_enum! {
    Macs,
    "hmac-md5" => HMacMd5,
    "hmac-sha1" => HMacSha1,
    "hmac-ripemd160" => HMacRipemd160,
    "umac-64@openssh.com" => UMac64,
    "hmac-sha1-96" => HMacSha1_96,
    "hmac-md5-96" => HMacMd5_96,
}

selection_enum! {
    PreferredAuthentications,
    "gssapi-with-mic" => GssapiWithMic,
    "hostbased" => HostBased,
    "publickey" => PublicKey,
    "keyboard-interactive" => KeyboardInteractive,
    "password" => Password,
}

selection_enum! {
    Protocol,
    "1" => V1,
    "2" => V2,
}

selection_enum! {
    StrictHostKeyChecking,
    "yes" => Yes,
    "no" => No,
    "ask" => Ask,
}

selection_enum! {
    VerifyHostKeyDns,
    "yes" => Yes,
    "no" => No,
    "ask" => Ask,
}

selection_enum! {
    Tunnel,
    "yes" => Yes,
    "point-to-point" => PointToPoint,
    "ethernet" => Ethernet,
    "no" => No,
}

selection_enum! {
    CompressionLevel,
    "1" => Level1,
    "2" => Level2,
    "3" => Level3,
    "4" => Level4,
    "5" => Level5,
    "6" => Level6,
    "7" => Level7,
    "8" => Level8,
    "9" => Level9,
}

selection_enum! {
    HostKeyAlgorithms,
    "ssh-rsa" => SshRsa,
    "ssh-dss" => SshDss,
}
