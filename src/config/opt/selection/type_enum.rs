/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#[path = "ident_count.rs"]
#[macro_use]
mod ident_count;

macro_rules! type_enum_no_display {
    ($name:ident, $variant:ident <> $($var:ident => $type:ty,)*) => {
        #[derive(PartialEq, Clone, Debug)]
        pub enum $name {
            $($var($type),)*
        }
        
        #[derive(PartialEq, Debug, Clone, Copy)]
        #[repr(C)]
        pub enum $variant {
            $($var,)*
        }
        
        impl $name {
            pub fn get_variant(&self) -> $variant {
                let variant = match self {
                    $(
                        &$name::$var(ref _x) => $variant::$var
                    ),*
                };
                variant
            }

            pub fn is_variant(&self, var: $variant) -> bool {
                self.get_variant() == var
            }
        }

        impl $variant {
            pub fn option_count() -> u32 {
                ident_count!($($var),*)
            }

            pub fn get_all() -> Vec<$variant> {
                let mut vec = Vec::<$variant>::new();
                $(vec.push($variant::$var);)*
                vec
            }

            #[allow(non_upper_case_globals)]
            pub fn as_str(&self) -> &'static CStr {
                match self {
                    $(&$variant::$var => {
                        const $var: &'static str = concat!(stringify!($var), "\0");
                        unsafe { & *($var as *const _ as *const CStr) }
                    },)*
                }
            }
        }
    }
}

#[allow(unused_macros)]
macro_rules! type_enum {
    ($name:ident, $variant:ident <> $($var:ident => $type:ty,)*) => {
        type_enum_no_display!{$name, $variant <> $($var => $type,)*}

        impl Display for $variant {
            fn fmt(&self, f: &mut Formatter) -> Result {
                Debug::fmt(self, f)
            }
        }
    };
}
