/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
pub mod defs;
pub use self::defs::*;

#[macro_use]
pub mod type_enum;
pub use self::type_enum::*;

use std::ffi::CStr;
use std::fmt::{Display,Debug,Result,Formatter};

macro_rules! all_selections_enum {
    ($($var:ident => $type:ty,)*) => {
        type_enum! {Selection, SelectionType <> $($var => $type,)*}

        impl Selection {
            pub fn from_int(ty: SelectionType, id: u8) -> Option<Selection> {
                match ty {
                    $(SelectionType::$var => match $var::from_int(id) {
                        Some(x) => Some(Selection::$var(x)),
                        None => None,
                    },)*
                }
            }
            pub fn from_int_vec(ty: SelectionType, ids: Vec<u8>) -> Vec<Selection> {
                let mut vec = Vec::<Selection>::new();
                for x in ids {
                    if x as i32 <= Selection::option_count(ty) {
                        match Selection::from_int(ty, x) {
                            Some(z) => vec.push(z),
                            None => (),
                        }
                    }
                }
                vec
            }
            pub fn option_count(ty: SelectionType) -> i32 {
                match ty {
                    $(SelectionType::$var => $var::option_count(),)*
                }
            }
            pub fn cstr_slice(ty: SelectionType) -> &'static [&'static CStr] {
                match ty {
                    $(SelectionType::$var => $var::cstr_slice(),)*
                }
            }
            pub fn as_int(&self) -> u8 {
                match self {
                    $(&Selection::$var(ref x) => *x as u8,)*
                }
            }
            pub fn as_cstr(&self) -> &'static CStr {
                match self {
                    $(&Selection::$var(ref x) => x.as_cstr(),)*
                }
            }
        }
        impl Display for Selection {
            fn fmt(&self, f: &mut Formatter) -> Result {
                let s = match self {
                    $(&Selection::$var(ref x) => x.to_string(),)*
                };
                f.write_str(&s)
            }
        }
    }
}

all_selections_enum! {
    Cipher                   => Cipher,
    ControlMaster            => ControlMaster,
    LogLevel                 => LogLevel,
    VerifyHostKeyDns         => VerifyHostKeyDns,
    StrictHostKeyChecking    => StrictHostKeyChecking,
    Tunnel                   => Tunnel,
    CompressionLevel         => CompressionLevel,
    HostKeyAlgorithms        => HostKeyAlgorithms,
    Ciphers                  => Ciphers,
    AddressFamily            => AddressFamily,
    KbdInteractiveDevices    => KbdInteractiveDevices,
    Macs                     => Macs,
    PreferredAuthentications => PreferredAuthentications,
    Protocol                 => Protocol,
}
