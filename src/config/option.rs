/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#[macro_use]
#[path = "opt/selection/type_enum.rs"]
mod type_enum;

use config::opt::*;
use config::opt::selection::*;

use std::ffi::{CString,CStr};
use std::fmt::{Display,Debug,Result,Formatter};

macro_rules! first {
    ($head:ident) => {
        ConfigOptionType::$head
    };
    
    ($head:ident, $($tail:ident,)*) => {
        ConfigOptionType::$head
    };
}

macro_rules! pub_enum_ConfigOption {
    (String         => $($str:ident,)*;
     Int            => $($int:ident,)*;
     Bool           => $($bool:ident,)*;
     Forward        => $($fd:ident,)*;
     Selection      => $($opt:ident,)*;
     SelectionList  => $($opt_list:ident,)*;) => {
        type_enum_no_display! {
            ConfigOption, ConfigOptionType
                <>
            $($str => CString,)*
            $($int => u32,)*
            $($bool => bool,)*
            $($fd => Forward,)*
            $($opt => $opt,)*
            $($opt_list => Vec<$opt_list>,)*
        }

        impl ConfigOptionType {
            pub fn get_opt_type(&self) -> OptType {
                let config_type = *self as u32;
                if config_type < first!($($int,)*) as u32 {
                    return OptType::String;
                } else if config_type < first!($($bool,)*) as u32 {
                    return OptType::Int;
                } else if config_type < first!($($fd,)*) as u32 {
                    return OptType::Bool;
                } else if config_type < first!($($opt,)*) as u32 {
                    return OptType::Forward;
                } else if config_type < first!($($opt_list,)*) as u32 {
                    return OptType::Selection;
                }
                OptType::SelectionList
            }

            pub fn get_selection_type(&self) -> Option<SelectionType> {
                match self {
                    $(&ConfigOptionType::$opt => Some(SelectionType::$opt),)*
                    $(&ConfigOptionType::$opt_list => Some(SelectionType::$opt_list),)*
                    _ => None,
                }
            }
        }

        impl Display for ConfigOptionType {
            fn fmt(&self, f: &mut Formatter) -> Result {
                if self == &ConfigOptionType::Invalid {
                    f.write_str(" ")
                } else if self == &ConfigOptionType::Comment {
                    f.write_str("#")
                } else {
                    Debug::fmt(self, f)
                }
            }
        }

        impl ConfigOption {
            pub fn get_opt_type(&self) -> OptType {
                self.get_variant().get_opt_type()
            }

            pub fn get_int(&self) -> Option<u32> {
                match self {
                    $(&ConfigOption::$int(ref x) => Some(x.clone()),)*
                    _ => None,
                }
            }
            
            pub fn get_bool(&self) -> Option<bool> {
                match self {
                    $(&ConfigOption::$bool(ref x) => Some(x.clone()),)*
                    _ => None,
                }
            }

            pub fn get_string(&self) -> Option<&CString> {
                match self {
                    $(&ConfigOption::$str(ref x) => Some(x),)*
                    _ => None,
                }
            }

            pub fn get_forward(&self) -> Option<&Forward> {
                match self {
                    $(&ConfigOption::$fd(ref x) => Some(x),)*
                    _ => None,
                }
            }

            pub fn get_selection(&self) -> Option<u8> {
                match self {
                    $(&ConfigOption::$opt(ref x) => Some(*x as u8),)*
                    _ => None,
                }
            }

            pub fn get_selection_list(&self) -> Option<Vec<u8>> {
                match self {
                    $(&ConfigOption::$opt_list(ref x) => {
                        let mut vec = Vec::<u8>::new();
                        for y in x {
                            vec.push(*y as u8);
                        }
                        Some(vec)
                    },)*
                    _ => None,
                }
            }

            pub fn from_int(ty: ConfigOptionType, val: u32) -> Option<ConfigOption> {
                match ty {
                    $(ConfigOptionType::$int => Some(ConfigOption::$int(val)),)*
                    _ => None,
                }
            }

            pub fn from_selection(ty: ConfigOptionType, val: u8) -> Option<ConfigOption> {
                match ty.get_selection_type() {
                    Some(_) => (),
                    None => { return None; },
                }
                match ty {
                    $(ConfigOptionType::$opt => match $opt::from_int(val) {
                        Some(x) => Some(ConfigOption::$opt(x)),
                        _ => None,
                    },)*
                    _ => None,
                }
            }

            pub fn from_bool(ty: ConfigOptionType, val: bool) -> Option<ConfigOption> {
                match ty {
                    $(ConfigOptionType::$bool => Some(ConfigOption::$bool(val)),)*
                    _ => None,
                }
            }

            pub fn from_string(ty: ConfigOptionType, val: &str) -> Option<ConfigOption> {
                match ty {
                    $(ConfigOptionType::$str => Some(ConfigOption::$str(CString::new(val).unwrap())),)*
                    _ => None,
                }
            }

            pub fn from_forward(ty: ConfigOptionType, val: Forward) -> Option<ConfigOption> {
                match ty {
                    $(ConfigOptionType::$fd => Some(ConfigOption::$fd(val)),)*
                    _ => None,
                }
            }

            pub fn from_selection_list(ty: ConfigOptionType, val: Vec<u8>) -> Option<ConfigOption> {
                match ty.get_selection_type() {
                    Some(_) => (),
                    None => { return None; },
                }
                match ty {
                    $(ConfigOptionType::$opt_list => {
                        let mut vec = Vec::<$opt_list>::new();
                        for x in val {
                            if let Some(x) = $opt_list::from_int(x) {
                                vec.push(x);
                            }
                        }
                        Some(ConfigOption::$opt_list(vec))
                    },)*
                    _ => None,
                }
            }
        }
        
        impl Display for ConfigOption {
            fn fmt(&self, f: &mut Formatter) -> Result {
                let string = match self.get_opt_type() {
                    OptType::Int => self.get_int().unwrap().to_string(),
                    OptType::Bool => { match self.get_bool().unwrap() {
                        true => String::from("yes"),
                        false => String::from("no"),                        
                    }},
                    OptType::String => self.get_string().unwrap().to_str().unwrap().to_owned(),
                    OptType::Forward => self.get_forward().unwrap().to_string(),
                    OptType::Selection => {
                        let sel_option = self.get_selection().unwrap();
                        let sel_type = self.get_variant().get_selection_type().unwrap();
                        let sel = Selection::from_int(sel_type, sel_option).unwrap();
                        sel.to_string()
                    },
                    OptType::SelectionList => {
                        let vec = self.get_selection_list().unwrap();
                        let mut s = String::from("");
                        let sel_type = self.get_variant().get_selection_type().unwrap();
                        let mut first = true;
                        for x in vec {
                            let sel = Selection::from_int(sel_type, x).unwrap();
                            if !first {
                                s += ",";
                            }
                            first = false;
                            s += &sel.to_string();
                        }
                        s
                    }
                };
                f.write_str(&string)
            }
        }
    };
}
// pub enum ConfigOption {
pub_enum_ConfigOption! {
    // OptType::CString
    String =>
        User,
    	Hostname,
    	BindAddress,
    	HostKeyAlias,
    	SmartcardDevice,
    	GssapiClientIdentity,
    	ProxyCommand,
    	EscapeChar,
    	ControlPath,
    	IdentityFile,
    	UserKnownHostsFile,
    	GlobalKnownHostsFile,
    	XAuthLocation,
    	LocalCommand,
    	SendEnv,
        DynamicForward,
    // extra types for comments and invalid options
    	Comment,
    	Invalid,
    ;
    Int =>
        Port,
    	RekeyLimit,
    	ConnectionAttempts,
    	ConnectTimeout,
    	ServerAliveCountMax,
    	ServerAliveInterval,
    	NumberOfPasswordPrompts,
    ;
    Bool =>
    	ChallengeResponseAuthentication,
    	PasswordAuthentication,
    	PubkeyAuthentication,
    	RsaAuthentication,
    	RhostsRsaAuthentication,
    	IdentitiesOnly,
    	HostbasedAuthentication,
    	KbdInteractiveAuthentication,
    	ForwardX11Trusted,
    	CheckHostIp,
    	NoHostAuthenticationForLocalhost,
    	HashKnownHosts,
    	EnableSshKeysign,
    	PermitLocalCommand,
    	ForwardAgent,
    	ForwardX11,
    	GssapiAuthentication,
    	GssapiKeyExchange,
    	GssapiDelegateCredentials,
    	GssapiRenewalForcesRekey,
    	GssapiTrustDns,
    	Compression,
    	TcpKeepAlive,
    	UsePrivilegedPort,
    	BatchMode,
    	ClearAllForwardings,
    	VisualHostKey,
    	GatewayPorts,
    	ExitOnForwardFailure,
    ;
    Forward =>
        LocalForward,
    	RemoteForward,
        TunnelDevice,
    ;
    Selection =>
        AddressFamily,
    	Cipher,
    	Tunnel,
    	CompressionLevel,
    	ControlMaster,
    	LogLevel,
    	StrictHostKeyChecking,
    	VerifyHostKeyDns,
    ;
    SelectionList =>
    	HostKeyAlgorithms,
    	Macs,
    	Protocol,
    	Ciphers,
    	PreferredAuthentications,
    	KbdInteractiveDevices,
    ;
}	
