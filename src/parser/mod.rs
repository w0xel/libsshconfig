/*
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
use std::fs::File;
use std::io::*;

use config::*;

use std::ffi::CString;
use std::fs;
use rand::*;
use rand::distributions::Alphanumeric;
use std::path; 

pub struct ConfigFile {
    path: String,
    configs: Vec<Config>,
}

impl ConfigFile {
    pub fn open(path: String) -> Result<ConfigFile> {
        match File::open(&path) {
            Ok(_) => Ok(ConfigFile{path: path, configs: Vec::<Config>::new()}),
            Err(why) => Err(why),
        }
    }

    pub fn add_config(&mut self, config: Config) -> &mut Config{
        let mut i = 0;
        for x in self.configs.iter() {
            if x.get_name() == config.get_name() {
                break;
            }
            i += 1;
        }
        if i < self.configs.len() {
            self.configs.remove(i);
        }
        self.configs.push(config);
        self.configs.last_mut().unwrap()
    }

    pub fn remove_config(&mut self, config: &Config) -> bool {
        let mut i = 0;
        for x in self.configs.iter() {
            if x as *const Config == config as *const Config {
                break;
            }
            i += 1;
        }
        if i < self.configs.len() {
            self.configs.remove(i);
            true
        } else {
            false
        }
    }

    pub fn get_config_by_index(&self, index: usize) -> Option<&Config> {
        if index < self.configs.len() {
            Some(&self.configs[index])
        } else {
            None
        }
    }

    pub fn get_config_by_name(&self, name: String) -> Option<&Config> {
        for x in self.configs.iter() {
            if x.get_name() == name {
                return Some(x)
            }
        }
        return None
    }

    pub fn create_or_open(path: String) -> Result<ConfigFile> {
        match File::open(&path) {
            Ok(_) => Ok(ConfigFile{path: path, configs: Vec::<Config>::new()}),
            Err(_) => {
                let path_clone = path.clone();
                let parent = path::Path::new(&path_clone).parent();
                match parent {
                    Some(par) =>  match fs::create_dir_all(par) {
                        Ok(_) => (),
                        Err(why) => return Err(why),
                    },
                    None => (),
                }
                
                match File::create(&path) {
                    Ok(_) => Ok(ConfigFile{path: path, configs: Vec::<Config>::new()}),
                    Err(why) => Err(why),
                }
            }
        }
    }
    
    pub fn write(&mut self) -> Result<()> {
        let mut rng = thread_rng();
        let filename: String = rng.sample_iter(&Alphanumeric).take(15).collect();
        let tmpfolder = "/tmp/libsshconfig";
        let res = fs::create_dir_all(tmpfolder);
        if let Err(why) = res {
            Err(why)
        } else {
            let filename = String::from(tmpfolder) + &filename;
            let tmp = File::create(&filename);
            match tmp {
                Ok(mut f) => {
                    let res = self.write_file(&mut f);
                    if let Err(why) = res {
                        Err(why)
                    } else {
                        let res = fs::copy(&filename, &self.path);
                        if let Err(why) = res {
                            Err(why)
                        } else {
                            let res = fs::remove_file(&filename);
                            match res {
                                Err(why) => {println!("Error: {:?}", why); Err(why)},
                                _ => Ok(()),
                            }
                        }
                    } 
                },
                Err(why) => Err(why),
            }
        }
    }

    fn write_file(&self, file: &mut File) -> Result<()> {
        let mut first = true;
        for x in self.configs.iter() {
            if !first {
                let res = file.write(b"\n");
                if let Err(why) = res {
                    return Err(why);
                }
            }
            first = false;
            let res = file.write_fmt(format_args!("Host {}\n", x.get_name()));
            if let Err(_) = res {
                return res;
            }
            let configs = x.get_options();
            for y in configs.iter() {
                let opt = y.to_string();
                let res = match y.get_variant() {
                    ConfigOptionType::Comment => {
                        let comment = String::from("	#") + &opt;
                        file.write_fmt(format_args!("{}\n", comment))
                    },
                    ConfigOptionType::Invalid => {
                        let invalid = String::from("	") + &opt;
                        file.write_fmt(format_args!("{}\n", invalid))
                    },
                    ty => {
                        let option = String::from("	") + &ty.to_string() + " " + &opt;
                        file.write_fmt(format_args!("{}\n", option))
                    },
                };
                if let Err(_) = res {
                    return res;
                }
            }
        }
        let res = file.flush();
        if let Err(_) = res {
            return res;
        }
        
        let res = file.sync_all();
        if let Err(_) = res {
            return res;
        }
        return Ok(());
    }

    pub fn parse(&mut self) -> Result<()> {
        let file = match File::open(&self.path) {
            Ok(f) => f,
            _ => match File::create(&self.path) {
                Ok(f) => f,
                Err(why) => return Err(why),
            },
        };

        for line in BufReader::new(&file).lines() {
            let s = match line {
                Ok(l) => l,
                Err(why) => return Err(why),
            };
            let s = s.trim();

            let lowercase = s.to_ascii_lowercase();
            if lowercase.starts_with("host ") {
                let s = s.split_at(5).1.trim().to_owned();
                self.configs.push(Config::new(s));
                continue;
            } else if s.starts_with("#") && self.configs.len() > 0 {
                let value_s = s.split_at(1);
                self.configs.last_mut().unwrap().add_option(ConfigOption::Comment(CString::new(value_s.1).unwrap()));
                continue;
            }
            let mut is_conf = false;
            for conf in ConfigOptionType::get_all().iter() {
                let conf_string = conf.to_string().to_owned() + " ";
                let conf_string = conf_string.to_ascii_lowercase();
                if lowercase.starts_with(&conf_string) {
                    is_conf = true;
                    let value_s = s.split_at(conf_string.len()).1.trim();
                    let config_option = match conf.get_opt_type() {
                        OptType::String => ConfigOption::from_string(*conf, value_s),
                        OptType::Int => {
                            match value_s.parse::<u32>() {
                                Ok(i) => ConfigOption::from_int(*conf, i),
                                _ => None,
                            }
                        },
                        OptType::Bool => {
                            match value_s {
                                "yes" => ConfigOption::from_bool(*conf, true),
                                "no" => ConfigOption::from_bool(*conf, false),
                                _ => None,
                            }
                        },
                        OptType::Forward => {
                            let mut iter = value_s.split_whitespace();
                            match iter.next() {
                                Some(from) => match iter.next() {
                                    Some(to) => match iter.next() {
                                        Some(_) => None,
                                        _ => ConfigOption::from_forward(*conf, Forward {from: CString::new(from).unwrap(), to: CString::new(to).unwrap()}),
                                    },
                                    _ => None,
                                },
                                _ => None,
                            }
                        },
                        OptType::Selection => {
                            let sel_type = conf.get_selection_type().unwrap();
                            let opts = Selection::cstr_slice(sel_type);
                            let c_str = CString::new(value_s).unwrap();
                            let res = opts.iter().position(|x| x == &c_str.as_c_str());
                            match res {
                                Some(opt) => ConfigOption::from_selection(*conf, opt as u8),
                                _ => None,
                            }
                        },
                        OptType::SelectionList => {
                            let sel_type = conf.get_selection_type().unwrap();
                            let opts = Selection::cstr_slice(sel_type);
                            let mut vec = Vec::<u8>::new();
                            let mut invalid = false;
                            for y in value_s.split(",") {
                                if y == "" {
                                    continue;
                                }
                                let c_str = CString::new(y).unwrap();
                                let res = opts.iter().position(|x| x == &c_str.as_c_str());
                                match res {
                                    Some(opt) => vec.push(opt as u8),
                                    _ => invalid = true,
                                }
                            }
                            if invalid {
                                None
                            } else {
                                ConfigOption::from_selection_list(*conf, vec)
                            }
                        },
                    };
                    if let Some(x) = config_option {
                        self.configs.last_mut().unwrap().add_option(x);
                    } else {
                        self.configs.last_mut().unwrap().add_option(ConfigOption::Invalid(CString::new(s).unwrap()));
                    }
                    break;
                } 
            }
            if !is_conf {
                if s.len() != 0 && self.configs.len() > 0 {
                    self.configs.last_mut().unwrap().add_option(ConfigOption::Invalid(CString::new(s).unwrap()));
                }
            }
        }
        return Ok(());
    }
}
