/* 
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef sshconfig_h
#define sshconfig_h

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

typedef enum {
  SSH_ADDRESS_FAMILY_ANY,
  SSH_ADDRESS_FAMILY_IP_V4,
  SSH_ADDRESS_FAMILY_IP_V6,
} SshAddressFamily;

typedef enum {
  SSH_CIPHER_BLOWFISH,
  SSH_CIPHER_TRIPPLE_DES,
  SSH_CIPHER_DES,
} SshCipher;

typedef enum {
  SSH_CIPHERS_AES128_CTR,
  SSH_CIPHERS_AES192_CTR,
  SSH_CIPHERS_AES256_CTR,
  SSH_CIPHERS_ARCFOUR256,
  SSH_CIPHERS_ARCFOUR128,
  SSH_CIPHERS_AES128_CBC,
  SSH_CIPHERS_TRIPPLEDES_CBC,
  SSH_CIPHERS_BLOWFISH_CBC,
  SSH_CIPHERS_CAST128_CBC,
  SSH_CIPHERS_AES192_CBC,
  SSH_CIPHERS_AES256_CBC,
  SSH_CIPHERS_ARCFOUR,
} SshCiphers;

typedef enum {
  SSH_COMPRESSION_LEVEL1,
  SSH_COMPRESSION_LEVEL2,
  SSH_COMPRESSION_LEVEL3,
  SSH_COMPRESSION_LEVEL4,
  SSH_COMPRESSION_LEVEL5,
  SSH_COMPRESSION_LEVEL6,
  SSH_COMPRESSION_LEVEL7,
  SSH_COMPRESSION_LEVEL8,
  SSH_COMPRESSION_LEVEL9,
} SshCompression;

typedef enum {
  SSH_CONTROL_MASTER_YES,
  SSH_CONTROL_MASTER_NO,
  SSH_CONTROL_MASTER_ASK,
  SSH_CONTROL_MASTER_AUTO,
  SSH_CONTROL_MASTER_AUTO_ASK,
} SshControlMaster;

typedef enum {
  SSH_HOST_KEY_ALGORITHMS_SSH_RSA,
  SSH_HOST_KEY_ALGORITHMS_SSH_DSS,
} SshHostKeyAlgorithms;

typedef enum {
  SSH_KBD_INTERACTIVE_DEVICES_BSD_AUTH,
  SSH_KBD_INTERACTIVE_DEVICES_PAM,
  SSH_KBD_INTERACTIVE_DEVICES_S_KEY,
} SshKbdInteractiveDevices;

typedef enum {
  SSH_KEY_USER,
  SSH_KEY_HOSTNAME,
  SSH_KEY_BIND_ADDRESS,
  SSH_KEY_HOST_KEY_ALIAS,
  SSH_KEY_SMARTCARD_DEVICE,
  SSH_KEY_GSSAPI_CLIENT_IDENTITY,
  SSH_KEY_PROXY_COMMAND,
  SSH_KEY_ESCAPE_CHAR,
  SSH_KEY_CONTROL_PATH,
  SSH_KEY_IDENTITY_FILE,
  SSH_KEY_USER_KNOWN_HOSTS_FILE,
  SSH_KEY_GLOBAL_KNOWN_HOSTS_FILE,
  SSH_KEY_X_AUTH_LOCATION,
  SSH_KEY_LOCAL_COMMAND,
  SSH_KEY_SEND_ENV,
  SSH_KEY_DYNAMIC_FORWARD,
  SSH_KEY_COMMENT,
  SSH_KEY_INVALID,
  SSH_KEY_PORT,
  SSH_KEY_REKEY_LIMIT,
  SSH_KEY_CONNECTION_ATTEMPTS,
  SSH_KEY_CONNECT_TIMEOUT,
  SSH_KEY_SERVER_ALIVE_COUNT_MAX,
  SSH_KEY_SERVER_ALIVE_INTERVAL,
  SSH_KEY_NUMBER_OF_PASSWORD_PROMPTS,
  SSH_KEY_CHALLENGE_RESPONSE_AUTHENTICATION,
  SSH_KEY_PASSWORD_AUTHENTICATION,
  SSH_KEY_PUBKEY_AUTHENTICATION,
  SSH_KEY_RSA_AUTHENTICATION,
  SSH_KEY_RHOSTS_RSA_AUTHENTICATION,
  SSH_KEY_IDENTITIES_ONLY,
  SSH_KEY_HOSTBASED_AUTHENTICATION,
  SSH_KEY_KBD_INTERACTIVE_AUTHENTICATION,
  SSH_KEY_FORWARD_X11_TRUSTED,
  SSH_KEY_CHECK_HOST_IP,
  SSH_KEY_NO_HOST_AUTHENTICATION_FOR_LOCALHOST,
  SSH_KEY_HASH_KNOWN_HOSTS,
  SSH_KEY_ENABLE_SSH_KEYSIGN,
  SSH_KEY_PERMIT_LOCAL_COMMAND,
  SSH_KEY_FORWARD_AGENT,
  SSH_KEY_FORWARD_X11,
  SSH_KEY_GSSAPI_AUTHENTICATION,
  SSH_KEY_GSSAPI_KEY_EXCHANGE,
  SSH_KEY_GSSAPI_DELEGATE_CREDENTIALS,
  SSH_KEY_GSSAPI_RENEWAL_FORCES_REKEY,
  SSH_KEY_GSSAPI_TRUST_DNS,
  SSH_KEY_COMPRESSION,
  SSH_KEY_TCP_KEEP_ALIVE,
  SSH_KEY_USE_PRIVILEGED_PORT,
  SSH_KEY_BATCH_MODE,
  SSH_KEY_CLEAR_ALL_FORWARDINGS,
  SSH_KEY_VISUAL_HOST_KEY,
  SSH_KEY_GATEWAY_PORTS,
  SSH_KEY_EXIT_ON_FORWARD_FAILURE,
  SSH_KEY_LOCAL_FORWARD,
  SSH_KEY_REMOTE_FORWARD,
  SSH_KEY_TUNNEL_DEVICE,
  SSH_KEY_ADDRESS_FAMILY,
  SSH_KEY_CIPHER,
  SSH_KEY_TUNNEL,
  SSH_KEY_COMPRESSION_LEVEL,
  SSH_KEY_CONTROL_MASTER,
  SSH_KEY_LOG_LEVEL,
  SSH_KEY_STRICT_HOST_KEY_CHECKING,
  SSH_KEY_VERIFY_HOST_KEY_DNS,
  SSH_KEY_HOST_KEY_ALGORITHMS,
  SSH_KEY_MACS,
  SSH_KEY_PROTOCOL,
  SSH_KEY_CIPHERS,
  SSH_KEY_PREFERRED_AUTHENTICATIONS,
  SSH_KEY_KBD_INTERACTIVE_DEVICES,
} SshKey;

typedef enum {
  SSH_LOG_LEVEL_QUIET,
  SSH_LOG_LEVEL_FATAL,
  SSH_LOG_LEVEL_ERROR,
  SSH_LOG_LEVEL_INFO,
  SSH_LOG_LEVEL_VERBOSE,
  SSH_LOG_LEVEL_DEBUG,
  SSH_LOG_LEVEL_DEBUG1,
  SSH_LOG_LEVEL_DEBUG2,
  SSH_LOG_LEVEL_DEBUG3,
} SshLogLevel;

typedef enum {
  SSH_MACS_H_MAC_MD5,
  SSH_MACS_H_MAC_SHA1,
  SSH_MACS_H_MAC_RIPEMD160,
  SSH_MACS_U_MAC64,
  SSH_MACS_H_MAC_SHA1_96,
  SSH_MACS_H_MAC_MD5_96,
} SshMacs;

typedef enum {
  SSH_PREFERRED_AUTHENTICATIONS_GSSAPI_WITH_MIC,
  SSH_PREFERRED_AUTHENTICATIONS_HOST_BASED,
  SSH_PREFERRED_AUTHENTICATIONS_PUBLIC_KEY,
  SSH_PREFERRED_AUTHENTICATIONS_KEYBOARD_INTERACTIVE,
  SSH_PREFERRED_AUTHENTICATIONS_PASSWORD,
} SshPreferredAuthentications;

typedef enum {
  SSH_PROTOCOL_V1,
  SSH_PROTOCOL_V2,
} SshProtocol;

typedef enum {
  SSH_SELECTION_TYPE_CIPHER,
  SSH_SELECTION_TYPE_CONTROL_MASTER,
  SSH_SELECTION_TYPE_LOG_LEVEL,
  SSH_SELECTION_TYPE_VERIFY_HOST_KEY_DNS,
  SSH_SELECTION_TYPE_STRICT_HOST_KEY_CHECKING,
  SSH_SELECTION_TYPE_TUNNEL,
  SSH_SELECTION_TYPE_COMPRESSION_LEVEL,
  SSH_SELECTION_TYPE_HOST_KEY_ALGORITHMS,
  SSH_SELECTION_TYPE_CIPHERS,
  SSH_SELECTION_TYPE_ADDRESS_FAMILY,
  SSH_SELECTION_TYPE_KBD_INTERACTIVE_DEVICES,
  SSH_SELECTION_TYPE_MACS,
  SSH_SELECTION_TYPE_PREFERRED_AUTHENTICATIONS,
  SSH_SELECTION_TYPE_PROTOCOL,
} SshSelectionType;

typedef enum {
  SSH_STRICT_HOST_KEY_CHECKING_YES,
  SSH_STRICT_HOST_KEY_CHECKING_NO,
  SSH_STRICT_HOST_KEY_CHECKING_ASK,
} SshStrictHostKeyChecking;

typedef enum {
  SSH_TUNNEL_YES,
  SSH_TUNNEL_POINT_TO_POINT,
  SSH_TUNNEL_ETHERNET,
  SSH_TUNNEL_NO,
} SshTunnel;

typedef enum {
  SSH_TYPE_BOOL,
  SSH_TYPE_INT,
  SSH_TYPE_STRING,
  SSH_TYPE_SELECTION,
  SSH_TYPE_SELECTION_LIST,
  SSH_TYPE_FORWARD,
} SshType;

typedef enum {
  SSH_VERIFY_HOST_KEY_DNS_YES,
  SSH_VERIFY_HOST_KEY_DNS_NO,
  SSH_VERIFY_HOST_KEY_DNS_ASK,
} SshVerifyHostKeyDns;

typedef struct SshConfig SshConfig;

typedef struct SshConfigFile SshConfigFile;

typedef struct SshConfigOption SshConfigOption;

typedef struct {
  const char *string;
  uintptr_t length;
} SshRustStr;

SshConfig *sshconfig_create(const char *name);

SshConfig *sshconfig_file_add_clone_config(SshConfigFile *file, const SshConfig *config);

SshConfig *sshconfig_file_add_new_config(SshConfigFile *file, const char *name);

void sshconfig_file_close(SshConfigFile *file);

SshConfigFile *sshconfig_file_create_or_open(const char *path);

const SshConfig *sshconfig_file_get_config_ref(const SshConfigFile *file, uint32_t index);

const SshConfig *sshconfig_file_get_config_ref_by_name(const SshConfigFile *file, const char *name);

SshConfigFile *sshconfig_file_open(const char *path);

bool sshconfig_file_parse(SshConfigFile *file);

bool sshconfig_file_remove_config(SshConfigFile *file, const SshConfig *config);

bool sshconfig_file_write(SshConfigFile *file);

void sshconfig_free(SshConfig *config);

const char *sshconfig_get_error(void);

const char *sshconfig_get_name(const SshConfig *config);

uint32_t sshconfig_get_option_count(const SshConfig *config);

uint32_t sshconfig_get_option_count_for_key(const SshConfig *config, SshKey key);

const SshConfigOption *sshconfig_get_option_ref(const SshConfig *config, uint32_t i);

const SshConfigOption *sshconfig_get_option_ref_by_key(const SshConfig *config, SshKey option_type);

const SshConfigOption *sshconfig_get_option_ref_for_key(SshConfig *config,
                                                        SshKey key,
                                                        uint32_t index);

uint32_t sshconfig_key_count(void);

SshSelectionType sshconfig_key_get_selection_type(SshKey option_type);

SshType sshconfig_key_get_type(SshKey ty);

const char *sshconfig_key_to_str(SshKey ty);

bool sshconfig_option_as_bool(const SshConfigOption *option);

const SshRustStr *sshconfig_option_as_forward(const SshConfigOption *option);

uint32_t sshconfig_option_as_int(const SshConfigOption *option);

uint8_t sshconfig_option_as_selection(const SshConfigOption *option);

uint32_t sshconfig_option_as_selection_list(const SshConfigOption *option, uint8_t *buf);

const char *sshconfig_option_as_str(const SshConfigOption *option);

SshKey sshconfig_option_get_key(const SshConfigOption *option);

uint32_t sshconfig_option_get_selection_list_length(const SshConfigOption *option);

SshType sshconfig_option_get_type(const SshConfigOption *option);

void sshconfig_remove_option(SshConfig *config, const SshConfigOption *option);

const char *sshconfig_selection_option_as_str(SshSelectionType selection, uint8_t option);

int32_t sshconfig_selection_type_get_option_count(SshSelectionType selection_type);

const SshRustStr *sshconfig_selection_type_get_option_strs(SshSelectionType opt);

void sshconfig_set_bool(SshConfig *config, SshKey ty, bool value);

void sshconfig_set_forward(SshConfig *config, SshKey ty, const char *from, const char *to);

void sshconfig_set_int(SshConfig *config, SshKey ty, uint32_t value);

void sshconfig_set_name(SshConfig *config, const char *name);

void sshconfig_set_selection(SshConfig *config, SshKey ty, uint8_t option);

void sshconfig_set_selection_list(SshConfig *config,
                                  SshKey ty,
                                  const uint8_t *options,
                                  uint8_t count);

void sshconfig_set_string(SshConfig *config, SshKey ty, const char *value);

bool sshconfig_unset(SshConfig *config, SshKey ty);

#endif /* sshconfig_h */
