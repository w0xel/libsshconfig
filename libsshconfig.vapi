/* 
Copyright 2018 Sebastian Würl

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

[CCode (cheader_filename = "sshconfig.h")]
namespace SshConfig {

	[CCode (cname = "SshKey", cprefix = "SSH_KEY_", has_type_id = false)]
	public enum Key {
		USER,
		HOSTNAME,
		BIND_ADDRESS,
		HOST_KEY_ALIAS,
		SMARTCARD_DEVICE,
		GSSAPI_CLIENT_IDENTITY,
		PROXY_COMMAND,
		ESCAPE_CHAR,
		CONTROL_PATH,
		IDENTITY_FILE,
		USER_KNOWN_HOSTS_FILE,
		GLOBAL_KNOWN_HOSTS_FILE,
		X_AUTH_LOCATION,
		LOCAL_COMMAND,
		SEND_ENV,
		DYNAMIC_FORWARD,
		COMMENT,
		INVALID,
		PORT,
		REKEY_LIMIT,
		CONNECTION_ATTEMPTS,
		CONNECT_TIMEOUT,
		SERVER_ALIVE_COUNT_MAX,
		SERVER_ALIVE_INTERVAL,
		NUMBER_OF_PASSWORD_PROMPTS,
		CHALLENGE_RESPONSE_AUTHENTICATION,
		PASSWORD_AUTHENTICATION,
		PUBKEY_AUTHENTICATION,
		RSA_AUTHENTICATION,
		RHOSTS_RSA_AUTHENTICATION,
		IDENTITIES_ONLY,
		HOSTBASED_AUTHENTICATION,
		KBD_INTERACTIVE_AUTHENTICATION,
		FORWARD_X11_TRUSTED,
		CHECK_HOST_IP,
		NO_HOST_AUTHENTICATION_FOR_LOCALHOST,
		HASH_KNOWN_HOSTS,
		ENABLE_SSH_KEYSIGN,
		PERMIT_LOCAL_COMMAND,
		FORWARD_AGENT,
		FORWARD_X11,
		GSSAPI_AUTHENTICATION,
		GSSAPI_KEY_EXCHANGE,
		GSSAPI_DELEGATE_CREDENTIALS,
		GSSAPI_RENEWAL_FORCES_REKEY,
		GSSAPI_TRUST_DNS,
		COMPRESSION,
		TCP_KEEP_ALIVE,
		USE_PRIVILEGED_PORT,
		BATCH_MODE,
		CLEAR_ALL_FORWARDINGS,
		VISUAL_HOST_KEY,
		GATEWAY_PORTS,
		EXIT_ON_FORWARD_FAILURE,
		LOCAL_FORWARD,
		REMOTE_FORWARD,
		TUNNEL_DEVICE,
		ADDRESS_FAMILY,
		CIPHER,
		TUNNEL,
		COMPRESSION_LEVEL,
		CONTROL_MASTER,
		LOG_LEVEL,
		STRICT_HOST_KEY_CHECKING,
		VERIFY_HOST_KEY_DNS,
		HOST_KEY_ALGORITHMS,
		MACS,
		PROTOCOL,
		CIPHERS,
		PREFERRED_AUTHENTICATIONS,
		KBD_INTERACTIVE_DEVICES,
	}

	[CCode (cname = "SshCipher", cprefix = "SSH_CIPHER_", has_type_id = false)]
	public enum Cipher {
		BLOWFISH,
		TRIPPLE_DES,
		DES,
	}

	[CCode (cname = "SshCiphers", cprefix = "SSH_CIPHERS_", has_type_id = false)]
	public enum Cipher2 {
		AES128_CTR,
		AES192_CTR,
		AES256_CTR,
		ARCFOUR256,
		ARCFOUR128,
		AES128_CBC,
		TRIPPLEDES_CBC,
		BLOWFISH_CBC,
		CAST128_CBC,
		AES192_CBC,
		AES256_CBC,
		ARCFOUR,
	}

	[CCode (cname = "SshCompression", cprefix = "SSH_COMPRESSION_", has_type_id = false)]
	public enum Compression {
		LEVEL1,
		LEVEL2,
		LEVEL3,
		LEVEL4,
		LEVEL5,
		LEVEL6,
		LEVEL7,
		LEVEL8,
		LEVEL9,
	}

	[CCode (cname = "SshCompression", cprefix = "SSH_COMPRESSION_", has_type_id = false)]
	public enum AddressFamily {
		ANY,
		IP_V4,
		IP_V6,
	}

	[CCode (cname = "SshControlMaster", cprefix = "SSH_CONTROL_MASTER_", has_type_id = false)]
	public enum ControlMaster {
		YES,
		NO,
		ASK,
		AUTO,
		AUTO_ASK,
	}

	[CCode (cname = "SshHostKeyAlgorithms", cprefix = "SSH_HOST_KEY_ALGORITHMS_", has_type_id = false)]
	public enum HostKeyAlgorithm {
		SSH_RSA,
		SSH_DSS,
	}
	
	[CCode (cname = "SshKbdInteractiveDevices", cprefix = "SSH_KBD_INTERACTIVE_DEVICES_", has_type_id = false)]
	public enum KbdInteractiveDevice {
		BSD_AUTH,
		PAM,
		S_KEY,
	}

	[CCode (cname = "SshMacs", cprefix = "SSH_MACS_", has_type_id = false)]
	public enum Mac {
		H_MAC_MD5,
		H_MAC_SHA1,
		H_MAC_RIPEMD160,
		U_MAC64,
		H_MAC_SHA1_96,
		H_MAC_MD5_96,
	}

	[CCode (cname = "SshPreferredAuthentications", cprefix = "SSH_PREFERRED_AUTHENTICATIONS_", has_type_id = false)]
	public enum AuthenticationMethod {
		GSSAPI_WITH_MIC,
		HOST_BASED,
		PUBLIC_KEY,
		KEYBOARD_INTERACTIVE,
		PASSWORD,
	}

	[CCode (cname = "SshProtocol", cprefix = "SSH_PROTOCOL_", has_type_id = false)]
	public enum Protocol {
		V1,
		V2,
	}

	[CCode (cname = "SshStrictHostKeyChecking", cprefix = "SSH_STRICT_HOST_KEY_CHECKING_", has_type_id = false)]
	public enum HostKeyChecking {
		YES,
		NO,
		ASK,
	}

	[CCode (cname = "SshTunnel", cprefix = "SSH_TUNNEL_", has_type_id = false)]
	public enum Tunnel {
		YES,
		POINT_TO_POINT,
		ETHERNET,
		NO,
	}

	[CCode (cname = "SshVerifyHostKeyDns", cprefix = "SSH_VERIFY_HOST_KEY_DNS_", has_type_id = false)]
	public enum VerifyHostKeyDNS {
		YES,
		NO,
		ASK,
	}

	[CCode (cname = "SshSelectionType", cprefix = "SSH_SELECTION_TYPE_", has_type_id = false)]
	public enum Selection {
		CIPHER,
		CONTROL_MASTER,
		LOG_LEVEL,
		VERIFY_HOST_KEY_DNS,
		STRICT_HOST_KEY_CHECKING,
		TUNNEL,
		COMPRESSION_LEVEL,
		HOST_KEY_ALGORITHMS,
		CIPHERS,
		ADDRESS_FAMILY,
		KBD_INTERACTIVE_DEVICES,
		MACS,
		PREFERRED_AUTHENTICATIONS,
		PROTOCOL,
	}

	[CCode (cname = "SshType", cprefix = "SSH_TYPE_", has_type_id = false)]
	public enum OptType {
		BOOL,
		INT,
		STRING,
		SELECTION,
		SELECTION_LIST,
		FORWARD,
	}

	[SimpleType]
	[IntegerType (rank = 9)]
	[CCode (cname = "uintptr_t", has_type_id = false)]
	public struct uintptr {
	}
	
	[CCode (cname = "SshRustStr", destroy_function = "", has_type_id = false)]
	public struct RustStr {
		string str;
		uintptr length;
	}

	[CCode (cname = "sshconfig_selection_type_get_option_count")]
	public static int getOptionCount(Selection selection_type);

	[CCode (cname = "sshconfig_selection_type_get_option_strs")]
	public static unowned RustStr[] getOptionStrs(Selection selection_type);
	
	[CCode (cname = "sshconfig_selection_option_as_str")]
	public static unowned string optionStr(Selection selection_type, uchar option);

	[CCode (cname = "sshconfig_key_get_type")]
	public static OptType getType(Key key);

	[CCode (cname = "sshconfig_key_to_str")]
	public static unowned string asStr(Key key);

	[CCode (cname = "SshConfigOption", free_function = "", has_type_id = false)]
	[Compact]
	public class Option {
		[CCode (cname = "sshconfig_option_as_bool")]
		public bool asBool();

		[CCode (cname = "sshconfig_option_as_int")]
		public uint asInt();

		[CCode (cname = "sshconfig_option_as_str")]
		public string asString();

		[CCode (cname = "sshconfig_option_as_forward")]
		public RustStr[] asForward();

		[CCode (cname = "sshconfig_option_as_selection")]
		public uchar asSelection();

		[CCode (cname = "sshconfig_option_as_selection_list")]
		public uint asSelectionList(out uchar[] buf);

		[CCode (cname = "sshconfig_option_get_selection_list_length")]
		public int getLength();

		[CCode (cname = "sshconfig_option_get_key")]
		public Key getKey();

		[CCode (cname = "sshconfig_option_get_type")]
		public OptType getType();
	}

	[CCode (cname = "SshConfig", free_function = "sshconfig_free")]
	[Compact]
	public class Config {
		[CCode (cname = "sshconfig_create")]
		public Config (string name);

		[CCode (cname = "sshconfig_get_name")]
		public unowned string getName();

		[CCode (cname = "sshconfig_get_option_count")]
		public int getOptionCount();

		[CCode (cname = "sshconfig_get_option_count_for_key")]
		public int getOptionCountForKey(Key key);

		[CCode (cname = "sshconfig_set_name")]
		public void setName(string name);

		[CCode (cname = "sshconfig_get_option_ref")]
		public unowned Option? getOption(int index);

		[CCode (cname = "sshconfig_get_option_ref_by_key")]
		public unowned Option? getOptionByKey(Key key);
		
		[CCode (cname = "sshconfig_get_option_ref_for_key")]
		public unowned Option? getOptionForKey(Key key, int index);

		[CCode (cname = "sshconfig_set_bool")]
		public void setBool(Key key, bool value);

		[CCode (cname = "sshconfig_set_int")]
		public void setInt(Key key, int value);

		[CCode (cname = "sshconfig_set_selection")]
		public void setSelection(Key key, uchar value);
		
		[CCode (cname = "sshconfig_set_selection_list")]
		public void setSelectionList(Key key, uchar[] values, int count);
		
		[CCode (cname = "sshconfig_set_string")]
		public void setString(Key key, string value);

		[CCode (cname = "sshconfig_set_forward")]
		public void setForward(Key key, string from, string to);

		[CCode (cname = "sshconfig_remove_option")]
		public void removeOption(Option? option);

		[CCode (cname = "sshconfig_unset")]
		public bool unset(Key key);
	}

	[CCode (cname = "sshconfig_get_error")]
	public static string getError();

	[CCode (cname = "sshconfig_key_count")]
	public static int totalKeyCount();
	
	
	[CCode (cname = "SshConfigFile", free_function = "sshconfig_file_close", has_type_id = false)]
	[Compact]
	public class File {
		[CCode (cname = "sshconfig_file_open")]
		public static File? open (string filename);

		[CCode (cname = "sshconfig_file_create_or_open")]
		public static File? createOrOpen (string filename);

		[CCode (cname = "sshconfig_file_add_new_config")]
		public unowned Config? addNewConfig(string name);

		[CCode (cname = "sshconfig_file_add_clone_config")]
		public unowned Config? addCloneConfig(Config config);

		[CCode (cname = "sshconfig_file_write")]
		public bool write();

		[CCode (cname = "sshconfig_file_parse")]
		public bool parse();

		[CCode (cname = "sshconfig_file_get_config_ref")]
		public unowned Config? getConfig(int index);
		
		[CCode (cname = "sshconfig_file_get_config_ref_by_name")]
		public unowned Config? getConfigByName(string name);

		[CCode (cname = "sshconfig_file_remove_config")]
		public bool removeConfig(Config? config);
	}
}